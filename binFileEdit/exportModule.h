
#ifndef EXPORT_MODULE_H__
#define EXPORT_MODULE_H__

#ifdef MAKEDLL
#define EXPORT_OBJECT __declspec(dllexport)
#else
#define EXPORT_OBJECT __declspec(dllimport)
#endif

#include <string>
#include <atlstr.h>
#include <memory>
#include <functional>

class EXPORT_OBJECT ExternalLib
{
public:
	using MessageCbType = std::function<void(void*, std::string)>;
	using MessageTarget = void*;

public:
	ExternalLib();
	virtual ~ExternalLib();

	static std::unique_ptr<ExternalLib> MakeInstance();
	virtual void Open(const std::string &filename, const std::string &keyword) = 0;
	virtual void Save(const CString &outStream) = 0;
	virtual void Clear() = 0;
	virtual CString ReadContent() = 0;

	virtual void Connection(MessageTarget target, MessageCbType cb) = 0;
};

#endif