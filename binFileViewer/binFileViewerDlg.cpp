
// binFileViewerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "binFileViewer.h"
#include "binFileViewerDlg.h"
#include "afxdialogex.h"

#include "exportModule.h"
#include "core.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CbinFileViewerDlg 대화 상자



CbinFileViewerDlg::CbinFileViewerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_BINFILEVIEWER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_core = std::make_unique<CoreUnit>();

	m_core->LibModuleInit(this, CbinFileViewerDlg::MessageCb);
}

CbinFileViewerDlg::~CbinFileViewerDlg()
{ }

void CbinFileViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, BINEDIT_EDITOR, m_editor);
	DDX_Control(pDX, BINEDIT_BTN_OPEN, m_openButton);
	DDX_Control(pDX, BINEDIT_BTN_SAVE, m_saveButton);
	DDX_Control(pDX, BINEDIT_BTN_GAMEDATA, m_gamedataSelect);
	DDX_Control(pDX, BINEDIT_BTN_MODIFIER, m_modifierSelect);
	DDX_Control(pDX, BINEDIT_BTN_MONSTER, m_monsterSelect);
	DDX_Control(pDX, BINEDIT_BTN_SOUNDSET, m_soundsetSelect);
	DDX_Control(pDX, BINEDIT_VSLIDER, m_verticalSlider);

	DDX_Control(pDX, BINEDIT_BTN_ERASE, m_eraseButton);
	DDX_Control(pDX, BINEDIT_FILENAME, m_topLabel);
	DDX_Control(pDX, BINEDIT_SEARCH_KEYINFO, m_searchInfoLabel);
	DDX_Control(pDX, BINEDIT_FIND, m_findEdit);
	DDX_Control(pDX, BINEDIT_BTN_FIND, m_findButton);

	ComponentLoadCompleted();
}

void CbinFileViewerDlg::ComponentLoadCompleted()
{
	m_openButton.EnableWindowsTheming(false);
	auto ButtonStyleGuide = [](CMFCButton &cButton, const CString &caption)
	{
		cButton.SetTextColor(RGB(255, 255, 255));
		cButton.SetFaceColor(RGB(64, 128, 128));
		cButton.SetWindowTextW(caption);
	};

	ButtonStyleGuide(m_openButton, CString(L"가져오기"));
	ButtonStyleGuide(m_saveButton, L"세이브");
	ButtonStyleGuide(m_modifierSelect, L"모디파이어");
	ButtonStyleGuide(m_gamedataSelect, L"게임데이터");
	ButtonStyleGuide(m_soundsetSelect, L"사운드 셋");
	ButtonStyleGuide(m_monsterSelect, L"괴물 데이터");
	ButtonStyleGuide(m_eraseButton, L"내용 지우기");
	ButtonStyleGuide(m_findButton, L"찾기");

	m_verticalSlider.SetRange(0, 3);
	m_topLabel.SetWindowTextW({});
	m_topLabel.SetBackgroundColor(RGB(240, 240, 240));
	m_topLabel.SetTextColor(RGB(163, 73, 164));

	m_searchInfoLabel.SetWindowTextW(L"검색 단축키 F3");
	m_searchInfoLabel.SetBackgroundColor(RGB(240, 240, 240));
	m_searchInfoLabel.SetTextColor(RGB(46, 92, 92));

	m_editor.SetBackgroundColor(FALSE, RGB(209, 236, 189));
	m_editor.LimitText(1048576);
}

void CbinFileViewerDlg::UpdateEditor()
{
	m_editor.SetSel(0, -1);
	m_editor.ReplaceSel(m_core->LoadContent());
	m_topLabel.SetWindowTextW(m_core->CurrentFilename());

	m_editor.SetSel(0, 0);
	m_editor.SetFocus();
}

void CbinFileViewerDlg::EmptyEditor()
{
	if (m_editor.GetWindowTextLengthW())
	{
		m_editor.SetSel(0, -1);
		m_editor.Clear();
		m_editor.SetSel(0, 0);
	}
	m_topLabel.SetWindowTextW({});
}

BEGIN_MESSAGE_MAP(CbinFileViewerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_BN_CLICKED(BINEDIT_BTN_OPEN, OnClickLoadData)
	ON_BN_CLICKED(BINEDIT_BTN_SAVE, OnClickSaveData)
	ON_BN_CLICKED(BINEDIT_BTN_ERASE, OnClickErase)
	ON_CONTROL_RANGE(BN_CLICKED, BINEDIT_BTN_GAMEDATA, BINEDIT_BTN_SOUNDSET, OnClickSubitem)
	ON_BN_CLICKED(BINEDIT_BTN_FIND, OnClickSearch)
	ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()


// CbinFileViewerDlg 메시지 처리기

BOOL CbinFileViewerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CbinFileViewerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CbinFileViewerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

void CbinFileViewerDlg::OnClickLoadData()
{
	CString szFilter(L"nox binary file (*.bin)|*.bin|All Files (*.*)|*.*||");
	CFileDialog fileDlg(TRUE, L"FileDialog", nullptr, OFN_HIDEREADONLY, szFilter.GetString(), this);
	if (fileDlg.DoModal() == IDOK)
	{
		std::string str;

		m_core->Open(fileDlg.GetPathName(), static_cast<BinFileTypes>(m_verticalSlider.GetPos() & 3));
	}
}

void CbinFileViewerDlg::OnClickSaveData()
{
	CString content;

	if (m_editor.GetWindowTextLengthW())
	{
		m_editor.GetWindowTextW(content);
		m_core->Save(content);
	}
}

void CbinFileViewerDlg::OnClickErase()
{
	m_core->Empty();
}

void CbinFileViewerDlg::OnClickSubitem(UINT nID)
{
	int sliderPos = 0;

	switch (nID)
	{
	case BINEDIT_BTN_GAMEDATA:
		break;
	case BINEDIT_BTN_MODIFIER:
		sliderPos = 1;
		break;
	case BINEDIT_BTN_MONSTER:
		sliderPos = 2;
		break;
	case BINEDIT_BTN_SOUNDSET:
		sliderPos = 3;
	}
	m_verticalSlider.SetPos(sliderPos);
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CbinFileViewerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CbinFileViewerDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN)
	{
		//if (pMsg->wParam == VK_RETURN) // ENTER키 눌릴 시
		//	return TRUE;
		//if (pMsg->wParam == VK_ESCAPE) // ESC키 눌릴 시
		//	return TRUE;
		switch (pMsg->wParam)
		{
		case VK_F3:
			m_findEdit.SetFocus();
			break;
		case VK_ESCAPE:
		case VK_F1:
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

HBRUSH CbinFileViewerDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
	switch (nCtlColor)
	{
	case CTLCOLOR_STATIC:
		pDC->SetTextColor(RGB(108, 49, 108));
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	default:
		return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	}
}

void CbinFileViewerDlg::OnClickSearch()
{
	CString search;
	long curStart = 0L, curEnd = 0L;

	m_findEdit.GetWindowTextW(search);
	m_editor.GetSel(curStart, curEnd);

	FINDTEXTEX ft;
	ft.chrg.cpMin = curStart+1;
	ft.chrg.cpMax = -1;
	ft.lpstrText = search;
	
	long result = m_editor.FindTextW(FR_DOWN, &ft);
	if (result != -1)
	{
		m_editor.SetSel(ft.chrgText);
		m_editor.SetFocus();
	}
	else
	{
		m_editor.SetSel(0, 0);
		//m_editor.SetFocus();
	}
}

void CbinFileViewerDlg::Listener(std::string &message)
{
	MessageID msgId = m_core->PickMessageID(message);
	
	switch (msgId)
	{
	case MessageID::MSG_CONTENT_NULL:
		::AfxMessageBox(L"파일현재 키 모드가 이 파일과 일치하지 않습니다");
		EmptyEditor();
		m_topLabel.SetWindowTextW(L"가져오기 실패!");
		break;
	case MessageID::MSG_LOAD_OK:
		UpdateEditor();
		break;
	case MessageID::MSG_WRITE_OK:
		::AfxMessageBox(L"파일 저장이 완료되었습니다");
		break;
	case MessageID::MSG_CLEAR_CTX:
		EmptyEditor();
	}
}

void CbinFileViewerDlg::MessageCb(void *ptr, std::string message)
{
	CbinFileViewerDlg *cWnd = static_cast<CbinFileViewerDlg *>(ptr);	//Todo. 올바르지 않은 구현!. 객체가 유효한지 판단불가

	cWnd->Listener(message);
}