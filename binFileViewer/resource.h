//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// binFileViewer.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_BINFILEVIEWER_DIALOG        102
#define IDR_MAINFRAME                   128
#define BINEDIT_EDITOR                  1000
#define BINEDIT_BTN_OPEN                1001
#define BINEDIT_BTN_SAVE                1002
#define BINEDIT_BTN_GAMEDATA            1003
#define BINEDIT_BTN_MODIFIER            1004
#define BINEDIT_BTN_MONSTER             1005
#define BINEDIT_BTN_SOUNDSET            1006
#define BINEDIT_VSLIDER                 1007
#define BINEDIT_BTN_ERASE               1008
#define BINEDIT_FILENAME                1009
#define BINEDIT_FIND                    1010
#define BINEDIT_BTN_FIND                1011
#define BINEDIT_SEARCH_KEYINFO          1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
