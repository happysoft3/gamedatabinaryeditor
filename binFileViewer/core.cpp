
#include "stdafx.h"
#include "core.h"
#include "exportModule.h"
#include "binHandler.h"
#include "utils\stringUtils.h"


CoreUnit::CoreUnit()
{ }

CoreUnit::~CoreUnit()
{ }

void CoreUnit::LibModuleInit(CDialogEx *cWnd, std::function<void(void*, std::string)> messageCb)
{
	InitKeyChecker();
	m_libModule = ExternalLib::MakeInstance();

	m_libModule->Connection(cWnd, messageCb);
}

CString CoreUnit::LoadContent()
{
	return m_libModule->ReadContent();
}

void CoreUnit::Open(const CString &filename, BinFileTypes type)
{
	std::string typeKey;

	switch (type)
	{
	case BinFileTypes::Gamedata:
		typeKey = "Gamedata";
		break;
	case BinFileTypes::Modifier:
		typeKey = "Modifier";
		break;
	case BinFileTypes::Monster:
		typeKey = "Monster";
		break;
	case BinFileTypes::Soundset:
		typeKey = "Soundset";
		break;
	}
	std::string destPath;
	std::wstring srcPath(filename.operator LPCWSTR());
	StringUtils::UnicodeToAnsi(srcPath, destPath);
	m_loadedFilename = filename;
	m_libModule->Open(destPath, typeKey);
}

void CoreUnit::Save(const CString &content)
{
	m_libModule->Save(content);
}

void CoreUnit::Empty()
{
	m_libModule->Clear();
}

CString CoreUnit::CurrentFilename() const
{
	return L"로드된 파일: " + m_loadedFilename;
}

void CoreUnit::InitKeyChecker()
{
	m_msgChecker.emplace("InvalidKey", MessageID::MSG_INVALID_KEY);
	m_msgChecker.emplace("LoadComplete", MessageID::MSG_LOAD_OK);
	m_msgChecker.emplace("Connected", MessageID::MSG_CONNECT_OK);
	m_msgChecker.emplace("EmptyContent", MessageID::MSG_CONTENT_NULL);
	m_msgChecker.emplace("WriteOK", MessageID::MSG_WRITE_OK);
	m_msgChecker.emplace("WriteNG", MessageID::MSG_WRITE_NG);
	m_msgChecker.emplace("WriteContextNull", MessageID::MSG_WRITE_CTX_NULL);
	m_msgChecker.emplace("ClearCtx", MessageID::MSG_CLEAR_CTX);
}

MessageID CoreUnit::PickMessageID(std::string &message)
{
	std::map<std::string, MessageID>::iterator msgIterator(m_msgChecker.find(message));

	if (msgIterator != m_msgChecker.end())
		return msgIterator->second;
	return MessageID::MSG_NOTHING;
}